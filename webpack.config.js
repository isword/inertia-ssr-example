const path = require('path');
const mix = require('laravel-mix')

require('laravel-mix-merge-manifest');

mix.mergeManifest();

module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
        },
    },
};
