# First Time Installation

```shell
# Install Composer packages
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php80-composer:latest \
    composer install --ignore-platform-reqs
    
# Install Node modules
docker run --rm \
    -v $(pwd):/usr/src/app \
    -w /usr/src/app \
    node:14 \
    npm install
    
# Compile frontend assets
docker run --rm \
    -v $(pwd):/usr/src/app \
    -w /usr/src/app \
    node:14 \
    npm run dev

# Proceed with regular Laravel setup
cp .env.example .env

vendor/bin/sail up

vendor/bin/sail artisan key:gen
vendor/bin/sail artisan migrate
```

# Operation

```shell
vendor/bin/sail up
vendor/bin/sail npm run watch
```
